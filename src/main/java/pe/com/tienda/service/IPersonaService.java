package pe.com.tienda.service;

import java.util.List;

import pe.com.tienda.model.Persona;

public interface IPersonaService {
	Persona registrar(Persona persona);

	void modificar(Persona persona);

	void eliminar(int idPersona);

	Persona listarId(int idPersona);

	List<Persona> listar();
}
