package pe.com.tienda.service;

import java.util.List;

import pe.com.tienda.model.Venta;

public interface IVentaService {
	Venta registrar(Venta venta);

	Venta listarId(int idVenta);

	List<Venta> listar();
}
