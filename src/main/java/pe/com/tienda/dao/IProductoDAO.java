package pe.com.tienda.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.com.tienda.model.Producto;

public interface IProductoDAO extends JpaRepository<Producto, Integer> {

}
