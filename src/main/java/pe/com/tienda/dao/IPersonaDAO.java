package pe.com.tienda.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.com.tienda.model.Persona;

public interface IPersonaDAO extends JpaRepository<Persona, Integer> {

}
