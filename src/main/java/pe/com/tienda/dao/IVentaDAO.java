package pe.com.tienda.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.com.tienda.model.Venta;

public interface IVentaDAO extends JpaRepository<Venta, Integer> {

}
